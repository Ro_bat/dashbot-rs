use serde::{Serialize, Deserialize};

pub type AttributeValue = String;

#[derive(Serialize, Deserialize,Clone)]
pub struct Attribute {
    pub name : String,
    pub value : Option<AttributeValue>
}

impl Attribute {
    pub fn new(name : &str, value : Option<AttributeValue>) -> Self {
        Self {
            name : name.to_owned(),
            value : value
        }
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn value(&self) -> Option<&AttributeValue> {
        match &self.value {
            Some(i) => Some(i),
            None => None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_attribute_string() {
        let attribute = Attribute::new("attribute_name", Some("42".to_string()) as Option<AttributeValue>);
        assert_eq!(attribute.name(), "attribute_name");
        assert_eq!(attribute.value(), Some(&"42".to_string()) as Option<&AttributeValue>);
    }
}
