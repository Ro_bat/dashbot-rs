mod commandline;
mod twitch;
mod matrix;
mod discord;

pub type CommandLineConnectorLoader = commandline::CommandLineConnectorLoader;
pub type TwitchConnectorLoader = twitch::TwitchConnectorLoader;
pub type MatrixConnectorLoader = matrix::MatrixConnectorLoader;

pub use crate::implementations::connectors::discord::DiscordConnectorLoader;