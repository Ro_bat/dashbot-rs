use crate::processing::*;
use crate::Module;
use crate::communication::*;
use crate::config::Configurable;
use async_trait::*;
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};
use crate::loader::{ModuleLoader,LoaderResult,LoaderError};
use serde::{Serialize, Deserialize};

// matrix sdk stuff
use std::convert::TryFrom;
use matrix_sdk::{
    Client, SyncSettings, Session,
    room::{Room},
    ruma::{UserId, events::{SyncMessageEvent, room::{message::MessageEventContent}}, RoomId},
};

use std::ops::Deref;

#[derive(Serialize, Deserialize)]
pub struct MatrixConnectorConfig {
    pub username : String,
    pub password: String,
    pub room_id : String,
    pub attempt_session_restore : bool,
    pub enable_read : bool,
    pub enable_write : bool,
    pub session : Option<Session>,
}

impl ::std::default::Default for MatrixConnectorConfig {
    fn default() -> Self { Self {
        username: "FULL_USER_NAME".to_owned(),
        password : "PASSWORD".to_owned(),
        room_id : "INTERNAL_ROOM_ID".to_owned(),
        attempt_session_restore : false,
        enable_read : true,
        enable_write : true,
        session : None
    }}
}

impl Configurable for MatrixConnectorConfig {}

pub struct MatrixConnector {
    client : Client,
    room : matrix_sdk::room::Joined,
    enable_write : bool,
    send_channel : Arc<Mutex<mpsc::Sender<Message>>>
}

impl MatrixConnector {
    pub async fn new(mut config : MatrixConnectorConfig, config_path : &std::path::Path, send_channel : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<Self> {
        let user = match UserId::try_from(config.username.clone()) {
            Ok(user) => user,
            Err(_) => return Err(LoaderError::UnknownConfigDirectory)
        };
        let client = match Client::new_from_user_id(user.clone()).await {
            Ok(client) => client,
            Err(_) => return Err(LoaderError::UnknownConfigDirectory)
        };
        {
            let localpart = user.localpart();
            let password = &config.password;
            let session = config.session.clone();
            if matches!(&session, Some(_x)) && config.attempt_session_restore {
                match session {
                    Some(session) => match client.restore_login(session).await {
                        Ok(_) => {
                        },
                        Err(_) => return Err(LoaderError::UnknownConfigDirectory)
                    }
                    _ => return Err(LoaderError::UnknownConfigDirectory) // this never happens.
                }
            }
            else {
                match client.login(localpart, password, None, None).await {
                    Ok(response) => {
                        config.session = Some(Session {
                            access_token : response.access_token,
                            device_id : response.device_id,
                            user_id : response.user_id,
                        });
                        match config.save_to_file(&config_path) {
                            Ok(_) => {},
                            Err(_) => {
                                eprintln!("Could not save Matrix Connector config to \"{}\".",config_path.display());
                            }
                        }
                    },
                    Err(_) => return Err(LoaderError::UnknownConfigDirectory)
                }
            }
        }
        let room_id = RoomId::try_from(&config.room_id as &str);
        let room_id = match room_id {
            Ok(id) => id,
            Err(_) => return Err(LoaderError::UnknownConfigDirectory)
        };
        let sync_response = client.sync_once(SyncSettings::default()).await;
        match sync_response {
            Ok(_) => (),
            Err(_) => return Err(LoaderError::UnknownConfigDirectory)
        }
        let room = client.get_joined_room(&room_id);
        let room = match room {
            Some(room) => room,
            None => return Err(LoaderError::UnknownConfigDirectory)
        };
        let connector = MatrixConnector {
            client : client,
            room : room,
            send_channel : Arc::clone(&send_channel),
            enable_write : config.enable_write
        };
        if config.enable_read {
            connector.client.register_event_handler({
                let room_id = room_id.clone();
                let send_channel = Arc::clone(&connector.send_channel);
                move |ev: SyncMessageEvent<MessageEventContent>, room : Room| {
                    let send_channel = Arc::clone(&send_channel);
                    let room_id = room_id.clone();
                    async move {
                        if let Room::Joined(joined_room) = room {
                            let message_room_id = joined_room.deref().deref().room_id();
                            if message_room_id != &room_id {
                                return ()
                            }
                            if let ruma_events::room::message::MessageType::Text(context) = ev.content.msgtype {
                                let user = User {
                                    name : String::from(ev.sender.as_str()),
                                    platform : String::from("Matrix"),
                                    qualifier : String::from(format!("matrix:{}", ev.sender.as_str())),
                                    power_level : PowerLevel::default()
                                };
                                let message = Message::from(&context.body, user, "!");
                                let transmitter = send_channel.lock().await;
                                match transmitter.send(message) {
                                    Ok(()) => (),
                                    Err(error) => { eprintln!("Couldn't send to channel. {}", error);}
                                };
                            }
                        }
                    }
                }
            }).await;
        }
        Ok(Arc::new(connector))
    }
}

#[async_trait]
impl Module for MatrixConnector {
    async fn send(&self, response: &Response) -> std::result::Result<(),ConnectorError> {
        if self.enable_write {
            match response {
                Response::Text(text) => {
                    let content = ruma_events::AnyMessageEventContent::RoomMessage(MessageEventContent::text_plain(text));
                    let response = self.room.send(content, None).await;
                    match response {
                        Ok(_) => Ok(()),
                        Err(_) => Err(ConnectorError::UnknownError)
                    }
                },
                Response::NotAllowed(_user) => {
                    //TODO: not yet supported :-)
                    Err(ConnectorError::NotImplemented)
                },
                Response::Ban(_user, _reason) => {
                    //TODO: not yet supported :-)
                    Err(ConnectorError::NotImplemented)
                },
            }
        } else {
            Ok(())
        }
    }

    async fn run(&self) {
        self.client.sync(SyncSettings::default()).await;
    }
}

pub struct MatrixConnectorLoader{}
#[async_trait]
impl ModuleLoader for MatrixConnectorLoader{
    fn name(&self) -> &str {
        std::any::type_name::<MatrixConnector>()
    }
    async fn load(&self, 
                  config_path : &std::path::Path,
                  tx : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module> {
        let matrix_config = MatrixConnectorConfig::load_from_file(config_path);
        match matrix_config {
            Ok(config) => {
                let matrix_connector = MatrixConnector::new(config, config_path, Arc::clone(&tx)).await;
                match matrix_connector {
                    Ok(connector) => Ok(connector as Arc<dyn Module>),
                    Err(error) => Err(error)
                }
            }
            Err(err) => Err(LoaderError::ConfigError(self.name().to_owned(),std::path::PathBuf::from(config_path), err))
        }
    }
}
