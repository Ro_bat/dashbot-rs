use crate::communication::{Message, Response, User, PowerLevel};
use std::sync::mpsc;
use std::sync::Arc;
use tokio::sync::{Mutex};
use crate::config::Configurable;
use async_trait::*;
use crate::Module;
use crate::processing::{ConnectorError};
use crate::loader::{ModuleLoader, LoaderResult, LoaderError};
use serde::{Serialize, Deserialize};
use discord::model::Event;
use discord::{Discord, Connection};

#[derive(Serialize, Deserialize)]
pub struct DiscordConnectorConfig {
    pub user_token : String,
    pub target_channel_ids : Vec<u64>,
    pub source_channel_ids : Vec<u64>
}

impl ::std::default::Default for DiscordConnectorConfig {
    fn default() -> Self { Self {
        user_token: "USER_TOKEN".to_owned(),
        target_channel_ids: Vec::new(),
        source_channel_ids: Vec::new()
    }}
}

impl Configurable for DiscordConnectorConfig {}

pub struct DiscordConnector {
    pub send_channel : Arc<Mutex<mpsc::Sender<Message>>>,
    pub config : DiscordConnectorConfig,
    pub discord : Discord,
    pub connection : Mutex<Connection>
}

impl DiscordConnector {
    pub async fn new(config : DiscordConnectorConfig, send_channel : Arc<Mutex<mpsc::Sender<Message>>>) -> Result<Self, LoaderError> {
        // Log in to Discord using a bot token from the environment
        let discord = match Discord::from_user_token(&config.user_token) {
            Ok(discord) => discord,
            Err(_) => {
                return Err(LoaderError::LoaderFailedError(std::any::type_name::<Self>().to_string(), "User token incorrect".to_string()));
            }
        };
    
        // Establish and use a websocket connection
        let connection = match discord.connect() {
            Ok((connection, _)) => connection,
            Err(_) => {
                return Err(LoaderError::LoaderFailedError(std::any::type_name::<Self>().to_string(), "Connection error".to_string()));
            }
        };
        Ok(Self {
            config,
            send_channel,
            discord,
            connection : Mutex::new(connection)
        })
    }
}

#[async_trait]
impl Module for DiscordConnector {
    async fn send(&self, text: &Response) -> Result<(),ConnectorError> {
        match text {
            Response::Text(text) => {
                for channel_id in &self.config.target_channel_ids {
                    let _ = self.discord.send_message(
                        discord::model::ChannelId(*channel_id),
                        text,
                        "",
                        false,
                    );
                }
            }
            _ => {}
        }
        Ok(())
    }
    
    async fn run(&self) {
        let mut connection = self.connection.lock().await;
        loop {
            match connection.recv_event() {
                Ok(Event::MessageCreate(message)) => {
                    if self.config.source_channel_ids.contains(&message.channel_id.0) {
                        let author = User {
                            name : message.author.name,
                            platform: "Discord".to_string(),
                            qualifier: format!("discord:{}", message.author.id),
                            power_level: PowerLevel::default()
                        };
                        let message = Message::from(&message.content, author, "!");
                        let sender = self.send_channel.lock().await;
                        match sender.send(message) {
                            Ok(()) => (),
                            Err(error) => { eprintln!("Couldn't send to channel. {}", error);}
                        };
                    }
                }
                Ok(_) => {}
                Err(discord::Error::Closed(code, body)) => {
                    eprintln!("Gateway closed on us with code {:?}: {}", code, body);
                    break;
                }
                Err(err) => eprintln!("Receive error: {:?}", err),
            }
        }
    }
}

pub struct DiscordConnectorLoader{}
#[async_trait]
impl ModuleLoader for DiscordConnectorLoader {
    fn name(&self) -> &str {
        std::any::type_name::<DiscordConnector>()
    }
    async fn load(&self, config_path : &std::path::Path, tx : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        let discord_config = DiscordConnectorConfig::load_from_file(config_path);
        match discord_config {
            Ok(config) => {
                let matrix_connector = DiscordConnector::new(config, Arc::clone(&tx)).await;
                match matrix_connector {
                    Ok(connector) => Ok(Arc::new(connector) as Arc<dyn Module>),
                    Err(error) => Err(error)
                }
            }
            Err(err) => Err(LoaderError::ConfigError(self.name().to_owned(),std::path::PathBuf::from(config_path), err))
        }
    }
}
