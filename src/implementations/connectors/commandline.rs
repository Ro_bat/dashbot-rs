use crate::communication::{Message, Response, User, PowerLevel};
use std::sync::mpsc;
use std::sync::Arc;
use tokio::sync::{Mutex};
use async_trait::*;
use crate::Module;
use crate::processing::{ConnectorError};
use crate::loader::{ModuleLoader,LoaderResult};
use tokio::io::{BufReader, AsyncBufReadExt};
use std::io::{self, Write};

pub struct CommandLineConnector {
    pub send_channel : Arc<Mutex<mpsc::Sender<Message>>>,
}

impl CommandLineConnector {
    async fn get_message(&self) -> Option<Message> {
        let user = User {
            name : String::from("Generic Command Line User"),
            platform : String::from("Commandline"),
            qualifier : String::from("commandline:user"),
            power_level : PowerLevel::default()
        };
        io::stdout().flush().unwrap();
        let stdin = tokio::io::stdin();
        let reader = BufReader::new(stdin);
        let mut lines = reader.lines();
        let line = match lines.next_line().await {
            Ok(line) => {
                match line {
                    Some(line) => {
                        let line = String::from(line.trim());
                        Some(line)
                    },
                    None => None
                }
            },
            Err(_) => None
        };  
        match line {
            Some(line) => {
                Some(Message::from(&line, user, "!"))
            },
            None => None
        }
    }
}

#[async_trait]
impl Module for CommandLineConnector {
    async fn send(&self, text: &Response) -> Result<(),ConnectorError> {
        match text {
            Response::Text(text) => {
                println!("Bot: {}", text);
                Ok(())
            },
            _ => Err(ConnectorError::NotImplemented)
        }
    }
    
    async fn run(&self) {
        loop {
            let message = self.get_message().await;
            match message {
                Some(m) => {
                    let transmitter = self.send_channel.lock().await;
                    match transmitter.send(m) {
                        Ok(()) => (),
                        Err(error) => {eprintln!("Couldn't send to channel. {}",error);}
                    }
                },
                None => ()
            }
        }
    }
}

pub struct CommandLineConnectorLoader{}
#[async_trait]
impl ModuleLoader for CommandLineConnectorLoader{
    fn name(&self) -> &str {
        std::any::type_name::<CommandLineConnector>()
    }
    async fn load(&self, _config_path : &std::path::Path, tx : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        Ok(Arc::new(CommandLineConnector{send_channel : Arc::clone(&tx)}))
    }
}
