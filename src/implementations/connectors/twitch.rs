use crate::processing::*;
use crate::Module;
use crate::communication::*;
use crate::config::Configurable;
use async_trait::*;
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};
use crate::loader::{ModuleLoader,LoaderResult,LoaderError};

//twitch_irc stuff
use twitch_irc::login::StaticLoginCredentials;
use twitch_irc::{ClientConfig,SecureTCPTransport,TwitchIRCClient};
use twitch_irc::message::ServerMessage;
use serde::{Serialize, Deserialize};
use std;

#[derive(Serialize, Deserialize)]
pub struct TwitchConnectorConfig {
    // https://dev.twitch.tv/docs/irc#overview
    pub target_channel : String,
    pub chat_bot_login_name: String,
    pub oauth_token : String
}

impl ::std::default::Default for TwitchConnectorConfig {
    fn default() -> Self { Self {
        target_channel: "TARGET_CHANNEL".into(),
        chat_bot_login_name : "CHAT_BOT_NAME".into(),
        oauth_token : "TOKEN_WITHOUT_OAUTH_COLON".into(),
    }}
}

impl Configurable for TwitchConnectorConfig {
}

pub struct TwitchConnector {
    client : Mutex<twitch_irc::TwitchIRCClient<twitch_irc::transport::tcp::TCPTransport<twitch_irc::transport::tcp::TLS>, twitch_irc::login::StaticLoginCredentials>>,
    incoming_messages : Mutex<tokio::sync::mpsc::UnboundedReceiver<twitch_irc::message::ServerMessage>>,
    send_channel : Arc<Mutex<mpsc::Sender<Message>>>,
    target_channel : String
}

impl TwitchConnector {
    pub fn new(connector_config : TwitchConnectorConfig, send_channel : Arc<Mutex<mpsc::Sender<Message>>>) -> TwitchConnector {
        let client_config = ClientConfig::new_simple(
            StaticLoginCredentials::new(connector_config.chat_bot_login_name, Some(connector_config.oauth_token))
        );

        let (incoming_messages, client) =
            TwitchIRCClient::<SecureTCPTransport, StaticLoginCredentials>::new(client_config);

        client.join(connector_config.target_channel.to_owned());
        TwitchConnector{
            client : Mutex::new(client),
            incoming_messages : Mutex::new(incoming_messages),
            send_channel : send_channel,
            target_channel : connector_config.target_channel
        }
    }
}

#[async_trait]
impl Module for TwitchConnector {
    async fn send(&self, response: &Response) -> Result<(),ConnectorError> {
        match response {
            Response::Text(text) => {
                match self.client.lock().await.say((&self.target_channel).to_owned(), text.clone()).await {
                    Ok(()) => Ok(()),
                    Err(error) => {
                        eprintln!("Twitch Connector Error: {}", error);
                        Err(ConnectorError::Other("Couldn't send to Twitch.".to_owned()))
                    }
                }
            },
            Response::Ban(user, reason) => {
                //TODO: not yet supported :-)
                match self.client.lock().await.ban((&self.target_channel).to_owned(), user.name(), Some(&reason)).await {
                    Ok(()) => Ok(()),
                    Err(error) => {
                        eprintln!("Twitch Connector Error: {}", error);
                        Err(ConnectorError::Other("Couldn't send to Twitch.".to_owned()))
                    }
                }
            },
            Response::NotAllowed(user) => {
                let text = format!("I'm sorry {}, I'm afraid I can't do that.", user.name());
                match self.client.lock().await.say((&self.target_channel).to_owned(), text).await {
                    Ok(()) => Ok(()),
                    Err(error) => {
                        eprintln!("Twitch Connector Error: {}", error);
                        Err(ConnectorError::Other("Couldn't send to Twitch.".to_owned()))
                    }
                }
            }
        }
    }

    async fn run(&self) {
        while let Some(message) = self.incoming_messages.lock().await.recv().await {
            match message {
                ServerMessage::Privmsg(message) => {
                    let mut power_level = PowerLevel::default();
                    for badge in &message.badges {
                        if badge.name=="admin" {
                            power_level = PowerLevel::admin()
                        }
                        else if badge.name=="moderator" {
                            power_level = PowerLevel::moderator()
                        }
                    }
                    let user = User {
                        name : String::from(message.sender.name),
                        platform : String::from("Twitch"),
                        qualifier : String::from(format!("twitch:{}",message.sender.id)),
                        power_level : power_level
                    };
                    let message = Message::from(&message.message_text, user, "!");
                    let transmitter = self.send_channel.lock().await;
                    match transmitter.send(message) {
                        Ok(()) => (),
                        Err(error) => { eprintln!("Couldn't send to channel. {}", error);}
                    }
                }
                _msg => { /*println!("{:?}", msg);*/ }
            }
        }
    }
}

pub struct TwitchConnectorLoader{}
#[async_trait]
impl ModuleLoader for TwitchConnectorLoader{
    fn name(&self) -> &str {
        std::any::type_name::<TwitchConnector>()
    }
    async fn load(&self, config_path : &std::path::Path, tx : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        let twitch_config = TwitchConnectorConfig::load_from_file(config_path);
        match twitch_config {
            Ok(config) => Ok(Arc::new(TwitchConnector::new(config, Arc::clone(&tx))) as Arc<dyn Module>),
            Err(err) => Err(LoaderError::ConfigError(self.name().to_owned(),std::path::PathBuf::from(config_path),err))
        }
    }
}
