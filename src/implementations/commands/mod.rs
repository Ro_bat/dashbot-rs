pub mod quotes;
pub mod system;
pub mod message;

pub type QuoteCProgressorLoader = quotes::QuoteCProgressorLoader;
pub type CommandCProgressorLoader = system::CommandCProgressorLoader;
pub type MessageCProcessorLoader = message::MessageCProcessorLoader;