use crate::Module;
use crate::config::Configurable;
use crate::communication::{Response, Message, CommandMessage, Command, User, PowerLevel};
use std::collections::{HashMap};
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};
use crate::environment::Environment;
use crate::loader::{ModuleLoader,LoaderResult,LoaderError};
use serde::{Serialize, Deserialize};
use async_trait::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct CommandResponse {
    command : Command,
    response : String
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MessageCommandProcessorConfig {
    responses : Vec<CommandResponse>
}

impl Default for MessageCommandProcessorConfig {
    fn default() -> Self { Self {
        responses : vec![CommandResponse {
            command: Command {
                command: "test".to_owned(),
                description: "Prints the test response.".to_owned(),
                required_power_level : PowerLevel::default()
            },
            response: "This is the test response.".to_owned()
        }]
    }}
}
impl Configurable for MessageCommandProcessorConfig {}

pub struct MessageCommandProcessor {
    responses : HashMap<String, CommandResponse>
}

impl MessageCommandProcessor {
    pub fn from(config : &MessageCommandProcessorConfig) -> Self {
        let responses = config.responses.iter()
                                        .map(|c|(c.command.command.clone(),c.clone()))
                                        .collect();
        return Self{
            responses : responses
        }
    }
}

#[async_trait]
impl Module for MessageCommandProcessor {
    async fn process_command(&self,
                             command : &CommandMessage,
                             author : &User,
                             _environment : &Environment) -> Option<Response> {
        let responder = self.responses.get(&command.name)?;
        if responder.command.available_to(&author) {
            Some(Response::from(responder.response.as_str()))
        } else {
            Some(Response::NotAllowed(author.clone()))
        }
    }
}

pub struct MessageCProcessorLoader{}
#[async_trait]
impl ModuleLoader for MessageCProcessorLoader{
    fn name(&self) -> &str {
        std::any::type_name::<MessageCommandProcessor>()
    }
    async fn load(&self,
                  config_path : &std::path::Path,
                  _incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        match MessageCommandProcessorConfig::load_from_file(config_path) {
            Ok(config) => Ok(Arc::new(MessageCommandProcessor::from(&config))),
            Err(err) => Err(LoaderError::ConfigError(std::any::type_name::<MessageCommandProcessor>().to_owned(),
                                                     config_path.to_owned(),
                                                     err))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::communication::User;
    #[tokio::test]
    async fn message_command_processor_default_text() {
        let processor = MessageCommandProcessor::from(&MessageCommandProcessorConfig {
            responses : vec![
                CommandResponse {
                    command: Command {
                        command: "test".to_owned(),
                        description: "Prints the test response.".to_owned(),
                        required_power_level : PowerLevel::default()
                    },
                    response: "This is the test response.".to_owned()
                },
                CommandResponse {
                    command: Command {
                        command: "test2".to_owned(),
                        description: "Prints another test response.".to_owned(),
                        required_power_level : PowerLevel::default()
                    },
                    response: "This is a completely different test response.".to_owned()
                },
                CommandResponse {
                    command: Command {
                        command: "privilege_test".to_owned(),
                        description: "This only works for users with elevated rights.".to_owned(),
                        required_power_level : PowerLevel::moderator()
                    },
                    response: "You have elevated rights.".to_owned()
                }]
        });
        let environment = Arc::new(Environment::new("test", vec![]));
        let user = User {
            name: "Test".to_owned(),
            platform: "Test".to_owned(),
            power_level: PowerLevel::default(),
            qualifier: "test:test".to_owned()
        };

        let user_elevated = User {
            name: "Test".to_owned(),
            platform: "Test".to_owned(),
            power_level: PowerLevel::moderator(),
            qualifier: "test:test".to_owned()
        };

        // test 1
        let command = CommandMessage {
            name: "test".to_owned(),
            args: vec![]
        };
        let response = processor.process_command(&command, &user, &environment).await;
        assert_eq!(response,Some(Response::from("This is the test response.")));

        // test 2
        let command = CommandMessage {
            name: "test2".to_owned(),
            args: vec![]
        };
        let response = processor.process_command(&command, &user, &environment).await;
        assert_eq!(response,Some(Response::from("This is a completely different test response.")));

        // test 3
        let command = CommandMessage {
            name: "privilege_test".to_owned(),
            args: vec![]
        };
        let response = processor.process_command(&command, &user, &environment).await;
        assert_eq!(response,Some(Response::NotAllowed(user.clone())));

        // test 4
        let command = CommandMessage {
            name: "privilege_test".to_owned(),
            args: vec![]
        };
        let response = processor.process_command(&command, &user_elevated, &environment).await;
        assert_eq!(response,Some(Response::from("You have elevated rights.")));
    }
}
