use crate::Module;
use crate::communication::{Message, Response, CommandMessage, User} ;
use std::process::Command;
use std::collections::HashSet;
use crate::environment::Environment;
use crate::loader::{ModuleLoader,LoaderResult};
use async_trait::*;
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};

// ***************
// System Process Subcommand Processor
// ***************
// This is a bad idea. I did this for a joke.

pub struct ProcessCommandCommandProcessor {}

impl ProcessCommandCommandProcessor {
    pub fn new() -> Self {
        let mut set = HashSet::<String>::new();
        set.insert("exec".to_owned());
        Self{}
    }
}

#[async_trait]
impl Module for ProcessCommandCommandProcessor {
    async fn process_command(&self,
                             command : &CommandMessage,
                             _author : &User,
                             _environment : &Environment) -> Option<Response> {
        if command.name == "exec" {
            let mut it = command.args.iter();
            let command_name = it.next()?;
            let args : Vec<&String> = it.collect();
            match Command::new(&command_name).args(&args).output() {
                Ok(output) => {
                    let text = String::from_utf8(output.stdout);
                    match text {
                        Ok(text) => return Some(Response::from(text.replace("\n",", "))),
                        Err(_) => return Some(Response::from("Error calling function."))
                    }
                }
                Err(_) => return Some(Response::from("Error calling function."))
            }
        }
        None
    }
}

pub struct CommandCProgressorLoader{}
#[async_trait]
impl ModuleLoader for CommandCProgressorLoader{
    fn name(&self) -> &str {
        std::any::type_name::<ProcessCommandCommandProcessor>()
    }
    async fn load(&self,
                  _config_path : &std::path::Path,
                  _incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        Ok(Arc::new(ProcessCommandCommandProcessor::new()))
    }
}
