use crate::config::Configurable;
use chrono::prelude::*;
use rand::Rng;
use serde::{Serialize, Deserialize};
use std::sync::{mpsc,Arc};
use std::collections::HashSet;
use crate::environment::Environment;
use crate::communication::{Response, Command, CommandMessage, User, Message, PowerLevel};
use crate::Module;
use crate::loader::{ModuleLoader,LoaderError,LoaderResult};
use async_trait::*;
use tokio::sync::Mutex;

// ***************
// Quote Subcommand Processor
// ***************

#[derive(Serialize, Deserialize, Clone)]
pub struct Quote {
    text: String,
    context: String,
    time: DateTime<Utc>
}

impl Quote {
    pub fn to_string(&self) -> String {
        format!("{} ({} - {})", self.text, self.context, self.time.format("%b %d, %Y %Z"))
    }

    pub fn contains(&self, text : &str) -> bool {
        self.text.to_lowercase().contains(&text)
    }
}

#[derive(Serialize, Deserialize)]
pub struct QuoteCommandProcessorConfig {
    quote_command : Command,
    add_quote_command : Command,
    delete_quote_command : Command,
    context_attribute : String,
    quotes : Vec<Quote>,
    no_quotes_message : String
}

impl Default for QuoteCommandProcessorConfig {
    fn default() -> Self { Self {
        quote_command : Command {
            command: "quote".to_owned(),
            description : "Calls an existing quote.".to_owned(),
            required_power_level : PowerLevel::default()
        },
        add_quote_command : Command {
            command: "addquote".to_owned(),
            description : "Adds a new quote.".to_owned(),
            required_power_level : PowerLevel::moderator()
        },
        delete_quote_command : Command {
            command: "delquote".to_owned(),
            description : "Deletes a quote.".to_owned(),
            required_power_level : PowerLevel::moderator()
        },
        context_attribute : "context".to_owned(),
        quotes : vec![],
        no_quotes_message : "There are no quotes.".to_owned(),
    }}
}

impl Configurable for QuoteCommandProcessorConfig {
}

pub struct QuoteCommandProcessor {
    config : Mutex<QuoteCommandProcessorConfig>,
    config_path : std::path::PathBuf,
}

impl QuoteCommandProcessor {
    pub fn from(config : QuoteCommandProcessorConfig, config_path : &std::path::Path) -> Self {
        let mut set = HashSet::<String>::new();
        set.insert(config.quote_command.command.clone());
        set.insert(config.add_quote_command.command.clone());
        set.insert(config.delete_quote_command.command.clone());
        Self{
            config : Mutex::new(config),
            config_path : config_path.to_owned()
        }
    }

    pub async fn quote(&self, args : &Vec<String>, _environment : &Environment) -> Option<Response> {
        let config = self.config.lock().await;
        if config.quotes.len() == 0 {
            Some(Response::from(config.no_quotes_message.clone()))
        }
        else {
            let text = args.join(" ");
            match text.parse::<usize>() {
                Ok(number) => { //if it's a number. Note: not the index.
                    let index = number - 1;
                    let quote = &config.quotes.get(index);
                    match quote {
                        Some(quote) => return Some(Response::from(format!("{}. {}", index+1, quote.to_string()))),
                        None => Some(Response::from(format!("There are only {} quotes.", config.quotes.len())))
                    }
                }
                Err(_) => {
                    if args.is_empty() {
                        let mut rng = rand::thread_rng();
                        let num = rng.gen_range(0..config.quotes.len());
                        return Some(Response::from(format!("{}. {}", num+1, config.quotes[num].to_string())))
                    }
                    else {
                        let mut matching_quotes = vec![];
                        let lowercase_text = text.to_lowercase();
                        for (index, quote) in config.quotes.iter().enumerate() {
                            if quote.contains(&lowercase_text) {
                                matching_quotes.push(index);
                            }
                        }
                        if matching_quotes.is_empty() {
                            Some(Response::from("No matching quotes."))
                        }
                        else {
                            let mut rng = rand::thread_rng();
                            let num = rng.gen_range(0..matching_quotes.len());
                            let num = matching_quotes[num];
                            Some(Response::from(format!("{}. {}", num+1, config.quotes[num].to_string())))
                        }
                    }
                }
            }
        }
    }

    pub async fn usage_response(&self) -> Option<Response> {
        let config = self.config.lock().await;
        Some(Response::from(format!("Usage: !{} Text | Speaker (optional) | Context (optional)", config.add_quote_command.command)))
    }

    pub async fn add_quote_internal(&self, quote_text : &str, person : Option<&str>, context: &str) -> Option<Response> {
        if quote_text.len() == 0 {
            return self.usage_response().await;
        }
        let text = {
            if quote_text.len()<2 {
                quote_text
            }
            else if quote_text.chars().next().unwrap()==quote_text.chars().last().unwrap() && 
                    (quote_text.chars().next().unwrap()=='\'' || quote_text.chars().next().unwrap()=='"') {
                &quote_text[1..quote_text.len()-1]
            }
            else {
                quote_text
            }
        };
        let full_text = if let Some(person) = person {
            if person.len() > 0 {
                format!("{}: \"{}\"", person, text)
            }
            else {
                format!("\"{}\"", text)
            }
        }
        else {
            format!("\"{}\"", text)
        };
        let new_quote = Quote {
            text: full_text,
            context: context.to_owned(),
            time: Utc::now()
        };
        let mut config = self.config.lock().await;
        config.quotes.push(new_quote);
        match config.save_to_file(&self.config_path) {
            Ok(()) => Some(Response::from(format!("Added quote {}: {}", config.quotes.len(), config.quotes[config.quotes.len()-1].to_string()))),
            Err(error) => {
                eprintln!("Error saving QuoteCommandProcessor config: {:?}",error);
                Some(Response::from(format!("Error saving QuoteCommandProcessor config: {:?}",error)))
            }
        }
    }

    pub async fn add_quote(&self, args : &Vec<String>, environment : &Environment) -> Option<Response> {
        let context_attribute = {
            let config = self.config.lock().await;
            config.context_attribute.clone()
        };
        let assumed_context = environment.attribute(&context_attribute).await.unwrap_or("Unknown".to_owned());
        match args.len() {
            1 => self.add_quote_internal(&args[0], None          , &assumed_context).await,
            2 => self.add_quote_internal(&args[0], Some(&args[1]), &assumed_context).await,
            3 => self.add_quote_internal(&args[0], Some(&args[1]), &args[2]).await,
            _ => self.usage_response().await
        }
    }

    pub async fn delete_quote(&self, args : &Vec<String>, _environment : &Environment) -> Option<Response> {
        let mut config = self.config.lock().await;
        if args.len() == 0 {
            Some(Response::from("Please specify a quote number."))
        }
        else {
            let numer_string = &args[0];
            match numer_string.parse::<usize>() {
                Ok(number) => {
                    let result = if (number > 0) && (number <= config.quotes.len()) {
                        let index = number - 1;
                        let result = Some(Response::from(format!("Deleted quote {}: {}", number, config.quotes[index].to_string())));
                        config.quotes.remove(index);
                        match config.save_to_file(&self.config_path) {
                            Ok(()) => (),
                            Err(error) => {eprintln!("Error saving QuoteCommandProcessor config: {:?}",error);}
                        }
                        result
                    } else {
                        Some(Response::from("Did not remove quote. Invalid quote number."))
                    };
                    result
                }
                Err(_) => Some(Response::from("Did not remove quote. Invalid quote number."))
            }
        }
    }
}

#[async_trait]
impl Module for QuoteCommandProcessor {

    async fn process_command(&self,
        command : &CommandMessage,
        author : &User,
        environment : &Environment) -> Option<Response> {
        let (quote_command, add_command, delete_command) = {
            let config = self.config.lock().await;
            (config.quote_command.clone(), 
             config.add_quote_command.clone(),
             config.delete_quote_command.clone())
        };
        if command.name == quote_command.command {
            if quote_command.available_to(author) {
                self.quote(&command.args, environment).await
            }
            else {
                Some(Response::NotAllowed(author.clone()))
            }
        }
        else if command.name == add_command.command {
            if add_command.available_to(author) {
                self.add_quote(&command.args, environment).await
            }
            else {
                Some(Response::NotAllowed(author.clone()))
            }
        }
        else if command.name == delete_command.command {
            if delete_command.available_to(author) {
                self.delete_quote(&command.args, environment).await
            }
            else {
                Some(Response::NotAllowed(author.clone()))
            }
        }
        else {
            None
        }
    }
}

pub struct QuoteCProgressorLoader{}
#[async_trait]
impl ModuleLoader for QuoteCProgressorLoader{
    fn name(&self) -> &str {
        std::any::type_name::<QuoteCommandProcessor>()
    }
    async fn load(&self,
                  config_path : &std::path::Path,
                  _incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        match QuoteCommandProcessorConfig::load_from_file(config_path) {
            Ok(config) => Ok(Arc::new(QuoteCommandProcessor::from(config, config_path))),
            Err(err) => Err(LoaderError::ConfigError(std::any::type_name::<QuoteCommandProcessor>().to_owned(),
                                                     config_path.to_owned(),
                                                     err))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempdir::TempDir;
    fn test_quotes() -> Vec<Quote> {
        let quote1 = Quote {
            text: "This is a test quote!".to_owned(),
            context: "Some Context".to_owned(),
            time: Utc.ymd(2020, 4, 20).and_hms(16, 20, 00)
        };
        let quote2 = Quote {
            text: "This is another test quote!".to_owned(),
            context: "Another Context".to_owned(),
            time: Utc.ymd(2020, 6, 22).and_hms(18, 22, 22)
        };
        let quote3 = Quote {
            text: "This is quote number three. This should be enough for testing.".to_owned(),
            context: "Third Context".to_owned(),
            time: Utc.ymd(2021, 1, 4).and_hms(19, 00, 00)
        };
        vec![quote1, quote2, quote3]
    }
    fn test_quote_config() -> QuoteCommandProcessorConfig {
        let config = QuoteCommandProcessorConfig {
            quote_command : Command {
                command: "quote".to_owned(),
                description : "Calls an existing quote.".to_owned(),
                required_power_level : PowerLevel::default()
            },
            add_quote_command : Command {
                command: "addquote".to_owned(),
                description : "Adds a new quote.".to_owned(),
                required_power_level : PowerLevel::moderator()
            },
            delete_quote_command : Command {
                command: "delquote".to_owned(),
                description : "Deletes a quote.".to_owned(),
                required_power_level : PowerLevel::moderator()
            },
            context_attribute : "context".to_owned(),
            quotes : test_quotes(),
            no_quotes_message : "There are no quotes.".to_owned(),
        };
        config
    }
    #[tokio::test]
    async fn quote_command_number_test() {
        let config = test_quote_config();
        let tmp_dir = TempDir::new("example").expect("create temp dir");
        let tmp_path = tmp_dir.path();
        let environment = Arc::new(Environment::new_overwrite_config_path("test",vec![],tmp_path));
        let config_path = environment.config_path();
        assert_ne!(config_path,None);
        if let Some(config_path) = config_path {
            let processor = QuoteCommandProcessor::from(config, &config_path);
            let command = CommandMessage {
                name : "quote".to_owned(),
                args : vec!["2".to_owned()]
            };
            let user = crate::communication::tests::test_user();
            let response = processor.process_command(&command, &user, &environment).await;
            assert_eq!(response,Some(Response::from("2. This is another test quote! (Another Context - Jun 22, 2020 UTC)")));
        }
    }

    /// helper function that runs the addquote command and returns the response last quote added
    async fn addquote_helper(args : &Vec<String>) -> Option<Quote> {
        let config = test_quote_config();
        let tmp_dir = TempDir::new("example").expect("create temp dir");
        let tmp_path = tmp_dir.path();
        let environment = Arc::new(Environment::new_overwrite_config_path("test",vec![],tmp_path));
        let config_path = environment.config_path();
        let result = if let Some(config_path) = config_path {
            let processor = QuoteCommandProcessor::from(config, &config_path);
            let command = CommandMessage {
                name : "addquote".to_owned(),
                args : args.clone()
            };
            let user = crate::communication::tests::test_user();
            let _response = processor.process_command(&command, &user, &environment).await;

            let last_quote = {
                let config = processor.config.lock().await;
                config.quotes.last().unwrap().clone()
            };
            Some(last_quote)
        } else {
            None
        };
        result
    }

    /// Tests the addquote functionality, providing text, person and context
    #[tokio::test]
    async fn quote_add_test() {
        let test_text = "A new quote to be added.".to_owned();
        let test_person = "Quote Person".to_owned();
        let test_context = "New Context".to_owned();
        let result_quote = addquote_helper(&vec![test_text.clone(),test_person.clone(),test_context.clone()]).await.unwrap();
        let expected_text = format!("{}: \"{}\"", test_person, test_text);
        assert_eq!(result_quote.text, expected_text);
        assert_eq!(result_quote.context, test_context);
    }

    /// Tests the addquote functionality, providing text, person (as empty string) and context
    #[tokio::test]
    async fn quote_add_test_empty_person() {
        let test_text = "A new quote to be added.".to_owned();
        let test_person = "".to_owned();
        let test_context = "New Context".to_owned();
        let result_quote = addquote_helper(&vec![test_text.clone(),test_person.clone(),test_context.clone()]).await.unwrap();
        let expected_text = format!("\"{}\"", test_text);
        assert_eq!(result_quote.text, expected_text);
        assert_eq!(result_quote.context, test_context);
    }

    /// Tests the addquote functionality, providing text, person (as empty string) and context
    #[tokio::test]
    async fn quote_add_test_only_text() {
        let test_text = "A new quote to be added.".to_owned();
        let result_quote = addquote_helper(&vec![test_text.clone()]).await.unwrap();
        let expected_text = format!("\"{}\"", test_text);
        assert_eq!(result_quote.text, expected_text);
        assert_eq!(result_quote.context, "Unknown");
    }
}
