mod fixed;
mod twitch;
mod manual;

pub type FixedAttributeLoader = fixed::FixedAttributeLoader;
pub type TwitchAttributeLoader = twitch::TwitchAttributeLoader;
pub type ManualAttributeLoader = manual::ManualAttributeLoader;
