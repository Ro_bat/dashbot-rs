use crate::Module;
use crate::attribute::{Attribute};
use crate::config::Configurable;
use crate::loader::{LoaderError, LoaderResult, ModuleLoader};
use crate::environment::Environment;
use crate::communication::{Message, Command, CommandMessage, User, Response, PowerLevel};
use serde::{Serialize, Deserialize};
use async_trait::*;
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};

#[derive(Serialize, Deserialize)]
pub struct ManualAttributeProviderConfig {
    attributes : Vec<Attribute>,
    set_command : Command,
    remove_command : Command,
}

impl ::std::default::Default for ManualAttributeProviderConfig {
    fn default() -> Self { Self {
        attributes : vec![Attribute::new("sample_manual_attribute",Some("Sample value".to_owned()))],
        set_command : Command {
            command: "set_attribute".to_owned(),
            description : "Sets an attribute.".to_owned(),
            required_power_level : PowerLevel::moderator()
        },
        remove_command : Command {
            command: "del_attribute".to_owned(),
            description : "Deletes an attribute.".to_owned(),
            required_power_level : PowerLevel::moderator()
        }
    }}
}

pub struct ManualAttributeProvider {
    config : Mutex<ManualAttributeProviderConfig>,
    config_path : std::path::PathBuf
}

impl ManualAttributeProvider {
    pub fn new(config : ManualAttributeProviderConfig, config_path : &std::path::Path) -> Self {
        Self {
            config : Mutex::new(config),
            config_path : config_path.to_owned()
        }
    }
}

#[async_trait]
impl Module for ManualAttributeProvider {
    async fn attributes(&self) -> Vec<Attribute> {
        let attributes = {
            let config = self.config.lock().await;
            config.attributes.clone()
        };
        attributes
    }
    async fn process_command(&self,
                             command : &CommandMessage,
                             _author : &User,
                             _environment : &Environment) -> Option<Response> {
        let mut config = self.config.lock().await;
        if command.name == config.set_command.command {
            if command.args.len() == 2 {
                let attribute_name = command.args[0].clone();
                let attribute_value = command.args[1].clone();
                let mut attribute = config.attributes.iter_mut()
                                          .filter(|attribute| attribute.name() == attribute_name);
                match attribute.next() {
                    Some(attribute) => {
                        attribute.value = Some(attribute_value.clone());
                        match config.save_to_file(&self.config_path) {
                            Ok(()) => {},
                            Err(err) => {eprintln!("Error saving {}: {:?}", self.config_path.to_str().unwrap(), err);}
                        }
                        Some(Response::from(format!("Set attribute '{}' with value '{}'.",
                                     attribute_name,
                                     attribute_value)))
                    }
                    None => {
                        config.attributes.push(Attribute::new(&attribute_name, Some(attribute_value.clone())));
                        match config.save_to_file(&self.config_path) {
                            Ok(()) => {},
                            Err(err) => {eprintln!("Error saving {}: {:?}", self.config_path.to_str().unwrap(), err);}
                        }
                        Some(Response::from(format!("Added new attribute '{}' with value '{}'.",
                                     attribute_name,
                                     attribute_value)))
                    }
                }
            } else {
                Some(Response::from(format!("Usage: {} <attribute name> | <attribute value>", config.set_command.command)))
            }
        }
        else if command.name == config.remove_command.command {
            if command.args.len() == 1 {
                let attribute_name = command.args[0].clone();
                config.attributes = config.attributes
                                          .iter()
                                          .filter(|attribute| attribute.name()!=attribute_name)
                                          .map(|attribute| attribute.clone())
                                          .collect();
                match config.save_to_file(&self.config_path) {
                    Ok(()) => {},
                    Err(err) => {eprintln!("Error saving {}: {:?}", self.config_path.to_str().unwrap(), err);}
                }
                Some(Response::from(format!("Removed attribute '{}'", attribute_name)))
            } else {
                Some(Response::from(format!("Usage: {} <attribute name>", config.remove_command.command)))
            }
        }
        else {
            None
        }
    }
}

impl Configurable for ManualAttributeProviderConfig {}

pub struct ManualAttributeLoader{}

#[async_trait]
impl ModuleLoader for ManualAttributeLoader{
    fn name(&self) -> &str {
        std::any::type_name::<ManualAttributeProvider>()
    }
    async fn load(&self, config_path : &std::path::Path, _incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        match ManualAttributeProviderConfig::load_from_file(config_path) {
            Ok(config) => Ok(Arc::new(ManualAttributeProvider::new(config, config_path))),
            Err(err) => {
                let name = ModuleLoader::name(self);
                Err(LoaderError::ConfigError(name.to_owned(),std::path::PathBuf::from(config_path), err))
            }
        }
    }
}
