use crate::attribute::{Attribute};
use crate::config::Configurable;
use crate::Module;
use crate::communication::{Message, PowerLevel, MessageType, DefaultChatMessage, User};
use crate::loader::{LoaderError, LoaderResult, ModuleLoader};
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};
use serde::{Serialize, Deserialize};
use async_trait::*;
use std::collections::HashMap;
use tokio::time::{sleep, Duration};
use chrono::prelude::*;

use twitch_api2::{TwitchClient};
use twitch_oauth2::{AccessToken, UserToken, client::reqwest_http_client};
use twitch_api2::types::UserName;
use twitch_api2::helix::games::GetGamesRequest;
use twitch_api2::types::CategoryId;
use twitch_api2::helix;
use twitch_api2::types;

#[derive(Serialize, Deserialize)]
pub struct TwitchAttributeProviderConfig {
    channel_name : String,
    access_token : String,
    notification_channels : Vec<String>,
    poll_interval_sec : u64,
    stream_timeout_min : i64,
    debug_output : bool
}

#[derive(PartialEq, typed_builder::TypedBuilder, Deserialize, Serialize, Clone, Debug)]
#[non_exhaustive]
pub struct GetReducedStreamsRequest {
    /// Cursor for forward pagination: tells the server where to start fetching the next set of results, in a multi-page response. The cursor value specified here is from the pagination response field of a prior query.
    #[builder(default)]
    pub after: Option<helix::Cursor>,
    /// Cursor for backward pagination: tells the server where to start fetching the next set of results, in a multi-page response. The cursor value specified here is from the pagination response field of a prior query.
    #[builder(default)]
    pub before: Option<helix::Cursor>,
    /// Maximum number of objects to return. Maximum: 100. Default: 20.
    #[builder(default)]
    pub first: Option<usize>,
    /// Returns streams broadcasting a specified game ID. You can specify up to 10 IDs.
    #[builder(default)]
    pub game_id: Vec<types::CategoryId>,
    /// Stream language. You can specify up to 100 languages.
    #[builder(default)]
    pub language: Option<String>,
    /// Returns streams broadcast by one or more specified user IDs. You can specify up to 100 IDs.
    #[builder(default, setter(into))]
    pub user_id: Vec<types::UserId>,
    /// Returns streams broadcast by one or more specified user login names. You can specify up to 100 names.
    #[builder(default)]
    pub user_login: Vec<types::UserName>,
}

#[derive(PartialEq, Deserialize, Serialize, Debug, Clone)]
#[non_exhaustive]
pub struct ReducedStream {
    /// ID of the game being played on the stream.
    pub game_id: CategoryId,
    /// Stream title.
    pub title: String,
    /// ID of the user who is streaming.
    pub user_id: types::UserId,
    /// Login of the user who is streaming.
    pub user_login: UserName
}

impl helix::Request for GetReducedStreamsRequest {
    type Response = Vec<ReducedStream>;

    const PATH: &'static str = "streams";
    const SCOPE: &'static [twitch_oauth2::Scope] = &[];
}

impl helix::RequestGet for GetReducedStreamsRequest {}

impl helix::Paginated for GetReducedStreamsRequest {
    fn set_pagination(&mut self, cursor: Option<helix::Cursor>) { self.after = cursor }
}


struct StreamInfo {
    pub last_seen: Option<DateTime<Utc>>
}

pub struct TwitchAttributeProvider<'a> {
    attributes : Mutex<Vec<Attribute>>,
    token : Mutex<Option<UserToken>>,
    client : TwitchClient<'a, reqwest::Client>,
    config : TwitchAttributeProviderConfig,
    stream_request : GetReducedStreamsRequest,
    send_channel : Arc<Mutex<mpsc::Sender<Message>>>,
    stream_info_map : Mutex<HashMap<UserName, StreamInfo>>,
    last_successful_stream_query : Mutex<Option<DateTime<Utc>>>,
    category_name_map : Mutex<HashMap<CategoryId, String>>
}

impl<'a> TwitchAttributeProvider<'a> {
    pub fn load(config : TwitchAttributeProviderConfig, send_channel : Arc<Mutex<mpsc::Sender<Message>>>) -> Self {
        let client = TwitchClient::new();
        let stream_request = GetReducedStreamsRequest::builder()
            .user_login(config.notification_channels.clone())
            .build();
        let mut stream_info = HashMap::new();
        for channel in config.notification_channels.iter() {
            stream_info.insert(String::clone(channel), StreamInfo {
                last_seen : None
            });
        }
        if config.debug_output {
            println!("[DiscordConnector] Started.");
        }
        Self {
            attributes : Mutex::new(vec![]),
            token: Mutex::new(None),
            client: client,
            config : config,
            stream_request,
            send_channel,
            stream_info_map : Mutex::new(stream_info),
            last_successful_stream_query : Mutex::new(None),
            category_name_map : Mutex::new(HashMap::new())
        }
    }
    async fn get_category_name_by_id(&self, id : &CategoryId, token : &twitch_oauth2::UserToken) -> Option<String> {
        let mut map = self.category_name_map.lock().await;
        match map.get(id) {
            Some(entry) => {
                Some(entry.clone())
            }
            None => {
                let request = GetGamesRequest::builder()
                    .id(vec![CategoryId::clone(id)])
                    .build();
                match self.client.helix.req_get(request, token).await {
                    Ok(result) => {
                        println!("request result: {:?}", result);
                        match result.data.first() {
                            Some(category) => {
                                map.insert(CategoryId::clone(id), category.name.clone());
                                Some(category.name.clone())
                            }
                            None => None
                        }
                    }
                    Err(_) => { eprintln!("Failed to query game."); None }
                }
            }
        }
    }
}

impl ::std::default::Default for TwitchAttributeProviderConfig {
    fn default() -> Self { Self {
        channel_name : "CHANNEL_NAME".to_owned(),
        access_token : "OAUTH_TOKEN".to_owned(),
        notification_channels : vec![],
        poll_interval_sec : 30,
        stream_timeout_min : 10,
        debug_output : false
    }}
}

#[async_trait]
impl<'a> Module for TwitchAttributeProvider<'a> {
    async fn attributes(&self) -> Vec<Attribute> {
        self.update().await;
        {
            let attributes = self.attributes.lock().await;
            attributes.iter().map(|a| a.clone()).collect()
        }
    }
    async fn update(&self) {
        let mut token = self.token.lock().await;
        if token.is_none() {
            if self.config.debug_output {
                println!("[DiscordConnector] Generating token.");
            }
            let token_result = UserToken::from_existing(
                reqwest_http_client,
                AccessToken::new(self.config.access_token.clone()),
                None, // Client ID
                None, // Client Secret
            ).await;
            *token = match token_result {
                Ok(token) => Some(token),
                _ => None
            };
        }
        if token.is_some() {
            if self.config.debug_output {
                println!("[DiscordConnector] Retrieving channel info.");
            }
            // get channel info, to retrieve game name
            let request = self.client.helix.get_channel_from_login(self.config.channel_name.clone(), &token.clone().unwrap()).await;
            match request {
                Ok(Some(result)) => {
                    let mut attributes = self.attributes.lock().await;
                    *attributes = vec![Attribute::new("game",Some(result.game_name.to_owned()))];
                }
                _ => {}
            }
            // get stream info, for notifications
            if self.config.notification_channels.len() > 0 {
                let result = self.client.helix.req_get(self.stream_request.clone(), &token.clone().unwrap()).await;
                match result {
                    Ok(response) => {
                        if self.config.debug_output {
                            println!("[DiscordConnector] Locking send_channel.");
                        }
                        let send_channel = self.send_channel.lock().await;
                        let current_time : DateTime<Utc> = Utc::now();
                        let mut last_successful_stream_query = self.last_successful_stream_query.lock().await;
                        let streams = response.data;
                        if self.config.debug_output {
                            println!("[DiscordConnector] Received response: {:?}", streams);
                        }
                        let mut stream_info_map = self.stream_info_map.lock().await;
                        for stream in streams.iter() {
                            let last_seen = stream_info_map[&stream.user_login].last_seen;
                            // make sure this isn't the first query.
                            if last_successful_stream_query.is_some() {
                                let last_seen = last_seen.unwrap_or(Utc.timestamp(0,0));
                                // if there was a successful query that didn't report the stream
                                // and if more than 10 minutes have passed since the stream was last seen,
                                // send a notification
                                if (last_seen < last_successful_stream_query.unwrap()) &&
                                   (current_time - last_seen).num_minutes() >= self.config.stream_timeout_min {
                                    let token = token.clone().unwrap();
                                    let game_name = self.get_category_name_by_id(&stream.game_id, &token).await;
                                    let text = match game_name {
                                        Some(game_name) => { format!("{} | {}\nhttps://www.twitch.tv/{}", stream.title, game_name, stream.user_login) }
                                        None => { format!("{}\nhttps://www.twitch.tv/{}", stream.title, stream.user_login) }
                                    };
                                    if self.config.debug_output {
                                        println!("[DiscordConnector] Sending message: {:?}", text);
                                    }
                                    let message = Message {
                                        message : MessageType::ChatMessage(Box::new(DefaultChatMessage{text})),
                                        author : User {
                                            name: "Twitch-Notification".to_string(),
                                            platform: "twitch-notify".to_string(),
                                            qualifier: format!("twitch-notify:{}", stream.user_id),
                                            power_level: PowerLevel::default()
                                        }
                                    };
                                    match send_channel.send(message) {
                                        Ok(()) => {},
                                        Err(_) => { eprintln!("TwitchAttributeProvider: Could not send message."); }
                                    }
                                }
                            }
                            // update last seen
                            stream_info_map.insert(String::clone(&stream.user_login), StreamInfo {
                                last_seen : Some(current_time)
                            });
                        }
                        // update last successful query
                        *last_successful_stream_query = Some(current_time);
                    },
                    Err(_) => { eprintln!("TwitchAttributeProvider: Failed to query twitch streams."); }
                }
            }
        } else {
            if self.config.debug_output {
                println!("[DiscordConnector] Failed to produce token.");
            }
        }
    }
    async fn run(&self) {
        if self.config.notification_channels.len() > 0 {
            if self.config.debug_output {
                println!("[DiscordConnector] Running...");
            }
            loop {
                if self.config.debug_output {
                    println!("[DiscordConnector] Waiting...");
                }
                sleep(Duration::from_millis(self.config.poll_interval_sec*1000)).await;
                self.update().await;
            }
        }
        if self.config.debug_output {
            println!("[DiscordConnector] Won't run, not set to notify.");
        }
    }
}

impl Configurable for TwitchAttributeProviderConfig {}

pub struct TwitchAttributeLoader{}

#[async_trait]
impl ModuleLoader for TwitchAttributeLoader {
    fn name(&self) -> &str {
        std::any::type_name::<TwitchAttributeProvider>()
    }
    async fn load(&self, config_path : &std::path::Path, incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        match TwitchAttributeProviderConfig::load_from_file(config_path) {
            Ok(config) => Ok(Arc::new(TwitchAttributeProvider::load(config, incoming_message_transmitter))),
            Err(err) => {
                let name = ModuleLoader::name(self);
                Err(LoaderError::ConfigError(name.to_owned(), 
                                             std::path::PathBuf::from(config_path),
                                             err))
            }
        }
    }
}
