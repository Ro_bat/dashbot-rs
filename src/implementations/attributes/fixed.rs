use crate::Module;
use crate::attribute::{Attribute};
use crate::config::Configurable;
use crate::loader::{LoaderError, LoaderResult, ModuleLoader};
use serde::{Serialize, Deserialize};
use async_trait::*;
use std::sync::{mpsc,Arc};
use tokio::sync::{Mutex};
use crate::communication::Message;

#[derive(Serialize, Deserialize)]
pub struct FixedAttributeProvider {
    attributes : Vec<Attribute>
}

impl FixedAttributeProvider {
    pub fn _new(attributes : Vec<Attribute>) -> Self {
        Self {
            attributes: attributes
        }
    }
}

impl ::std::default::Default for FixedAttributeProvider {
    fn default() -> Self { Self {
        attributes : vec![Attribute::new("sample_attribute",None)]
    }}
}

#[async_trait]
impl Module for FixedAttributeProvider {
    async fn attributes(&self) -> Vec<Attribute> {
        self.attributes.iter().map(|a| (*a).clone()).collect()
    }
}

impl Configurable for FixedAttributeProvider {}

pub struct FixedAttributeLoader{}

#[async_trait]
impl ModuleLoader for FixedAttributeLoader{
    fn name(&self) -> &str {
        std::any::type_name::<FixedAttributeProvider>()
    }
    async fn load(&self, config_path : &std::path::Path, _incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>{
        match FixedAttributeProvider::load_from_file(config_path) {
            Ok(provider) => Ok(Arc::new(provider)),
            Err(err) => {
                let name = ModuleLoader::name(self);
                Err(LoaderError::ConfigError(name.to_owned(),std::path::PathBuf::from(config_path), err))
            }
        }
    }
}
