pub mod commands;
pub mod connectors;
pub mod processors;
pub mod attributes;

use crate::loader::*;
use crate::implementations::connectors::*;
use crate::implementations::commands::*;
use crate::implementations::processors::*;
use crate::implementations::attributes::*;

pub fn module_loader() -> Box<dyn Loader> {
    let modules : Vec<Box<dyn ModuleLoader>> =
        vec![Box::new(FixedAttributeLoader{}),
             Box::new(TwitchAttributeLoader{}),
             Box::new(CommandLineConnectorLoader{}),
             Box::new(MatrixConnectorLoader{}),
             Box::new(DiscordConnectorLoader{}),
             Box::new(TwitchConnectorLoader{}),
             Box::new(EchoProcessorLoader{}),
             Box::new(MessageCProcessorLoader{}),
             Box::new(CommandCProgressorLoader{}),
             Box::new(QuoteCProgressorLoader{}),
             Box::new(ManualAttributeLoader{})];
    Box::new(DefaultLoader::new(modules))
}
