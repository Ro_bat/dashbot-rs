use crate::Module;
use crate::communication::*;
use crate::environment::Environment;
use crate::loader::{ModuleLoader,LoaderResult};
use std::sync::Arc;
use std::sync::mpsc;
use tokio::sync::{Mutex};
use async_trait::*;

pub struct EchoProcessor {
}

#[async_trait]
impl Module for EchoProcessor {
    async fn process_chat(&self, message : &dyn ChatMessage, _author : &User, _environment : &Environment) -> Option<Response> {
        Some(Response::from(message.text()))
    }
}

pub struct EchoProcessorLoader{}
#[async_trait]
impl ModuleLoader for EchoProcessorLoader{
    fn name(&self) -> &str{
        std::any::type_name::<EchoProcessor>()
    }
    async fn load(&self, _config_path : &std::path::Path, _incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module> {
        Ok(Arc::new(EchoProcessor{}))
    }
}
