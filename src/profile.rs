use serde::{Serialize, Deserialize};
use crate::environment::*;
use crate::config::Configurable;
use tokio::sync::{Mutex};
use std::sync::Arc;
use crate::Module;
use crate::processing::{ConnectorError};
use crate::loader::*;
use std::sync::mpsc;
use crate::communication::{Message,MessageType,Response,ChatMessage,CommandMessage,User};

#[derive(Serialize, Deserialize, Clone)]
pub struct ModuleData {
    name : String,
    config_file :  String
}

impl ModuleData {
    pub fn new(name : &str, config_file : &str) -> Self {
        ModuleData {
            name : name.to_owned(),
            config_file : config_file.to_owned()
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ProfileData {
    name : String,
    modules : Vec<ModuleData>,
}

impl ProfileData {
    pub fn new(name : &str, modules : Vec<ModuleData>) -> Self {
        ProfileData {
            name : name.to_owned(),
            modules
        }
    }
}

impl ::std::default::Default for ProfileData {
    fn default() -> Self { Self {
            name : "default".to_owned(),
            modules : vec![ModuleData::new("dashbot::implementations::attributes::fixed::FixedAttributeProvider<alloc::string::String>", "static_attributes.yaml"),
                           ModuleData::new("dashbot::implementations::commands::message::MessageCommandProcessor", "static_messages.yaml")]
    }}
}
impl Configurable for ProfileData {}

#[derive(Serialize, Deserialize)]
pub struct ProfileDataCollection {
    pub profiles : Vec<ProfileData>
}

impl ProfileDataCollection {
    pub async fn run(&self, loader : Arc<Mutex<Box<dyn Loader>>>) {
        let tasks : Vec<_> = self.profiles
                                 .iter()
                                 .map(|p| {
                                      let loader = Arc::clone(&loader);
                                      let profile = p.clone();
                                      tokio::spawn(async move {
                                          let mut instance = ProfileInstance::load(&profile, loader.lock().await.as_ref()).await.expect("Could not load something.");
                                          instance.run().await;
                                      })
                                   })
                                  .collect();
        futures::future::try_join_all(tasks).await.unwrap(); //TODO: error handling
    }
}

impl ::std::default::Default for ProfileDataCollection {
    fn default() -> Self { Self {
        profiles : vec![ProfileData::default()]
    }}
}

impl Configurable for ProfileDataCollection {}

pub struct ProfileInstance {
    environment : Arc<Environment>,
    _sender : Arc<Mutex<mpsc::Sender<Message>>>,
    receiver : Arc<Mutex<mpsc::Receiver<Message>>>
}

impl ProfileInstance {
    pub async fn load(profile : &ProfileData, loader : &dyn Loader) -> Result<ProfileInstance,LoaderError> {
        let (tx, rx) = mpsc::channel();
        let tx : Arc<Mutex<mpsc::Sender<Message>>> = Arc::new(Mutex::new(tx));
        let rx : Arc<Mutex<mpsc::Receiver<Message>>> = Arc::new(Mutex::new(rx));
        // instantiate environment
        let mut environment = Environment::new(&profile.name, vec![]); // TODO, but attribute providers aren't supported by Loader yet
        let base_config_path = match environment.config_path() {
            Some(path) => path,
            None => return Err(LoaderError::UnknownConfigDirectory)
        };
        // instantiate attribute providers
        let path_repeater = std::iter::repeat(base_config_path.clone());
        let channel_repeater = std::iter::repeat(Arc::clone(&tx));
        let modules = profile.modules.iter().zip(path_repeater).zip(channel_repeater)
            .map(|((a,path),incoming_message_transmitter)| async move {
               let mut config_path = path.clone();
               config_path.push(&a.config_file);
               let processor = loader.load_module_from_file(&a.name, &config_path, incoming_message_transmitter).await;
               match processor {
                   Ok(i) => Ok(i),
                   Err(error) => return Err(error)
               }
            });
        let modules = futures::future::join_all(modules).await;
        let mut modules_tmp = vec![];
        for module in modules {
            match module {
                Ok(module) => {modules_tmp.push(module)},
                Err(error) => {return Err(error);}
            }
        };
        environment.modules = modules_tmp;

        Ok(ProfileInstance {
            environment : Arc::new(environment),
            _sender : tx,
            receiver : rx
        })
    }
    pub async fn run(&mut self) {
        let mut futures = vec![];
        // run connectors
        for module in self.environment.modules.iter() {
            let connector = Arc::clone(module);
            futures.push(tokio::spawn(async move {
                connector.run().await;
            }));
        }
        let receiver = Arc::clone(&self.receiver);
        let environment = Arc::clone(&self.environment);
        let modules = self.environment.modules.clone();
        futures.push(tokio::spawn(async move {
            loop {
                let message = {
                    let lock = receiver.lock().await;
                    lock.recv().unwrap()
                };
                let response_text = Self::get_response(&message,
                                                       &environment ).await;
                Self::send_response(response_text, &modules).await;
            }}));
        futures::future::try_join_all(futures).await.unwrap();
    }
    async fn get_response(message : &Message,
                          environment : &Arc<Environment>) -> Option<Response> {
        match &message.message {
            MessageType::ChatMessage(chat_message) => {return Self::get_chat_response(chat_message.as_ref(), &message.author, environment).await;}
            MessageType::CommandMessage(command) => {return Self::get_command_response(&command, &message.author, environment).await;} //TODO: implement.
        }
    }
    async fn get_chat_response(message : &dyn ChatMessage, author : &User, environment : &Arc<Environment>) -> Option<Response> {
        for module in &environment.modules {
            match module.process_chat(message, author, environment).await {
                None => (),
                Some(text) => {return Some(text)}
            }
        }
        None
    }
    async fn get_command_response(command : &CommandMessage, author : &User, environment : &Arc<Environment>) -> Option<Response> {
        for module in &environment.modules {
            match module.process_command(command, author, environment).await {
                None => (),
                Some(text) => {return Some(text)}
            }
        }
        None
    }
    async fn send_response(response : Option<Response>, connectors : &Vec<Arc<dyn Module>>) {
        match response {
            Some(text) => {
                for connector in connectors.iter() {
                    match connector.send(&text).await {
                        Ok(()) => (),
                        Err(ConnectorError::NotImplemented) => { /* Module does not implement sending. Intended behavior. */}
                        Err(error) => { eprintln!("Send error: {:?}",error); }
                    }
                }
            }
            None => ()
        }
    }
}
