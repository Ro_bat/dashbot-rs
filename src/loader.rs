use std::sync::mpsc;
use tokio::sync::{Mutex};
use std::sync::Arc;
use crate::communication::Message;
use crate::Module;
use crate::config::ConfigError;
use std::path::PathBuf;
use async_trait::*;

#[derive(Debug)]
pub enum LoaderError {
    LoaderDoesNotExist(String),
    ConfigError(String, PathBuf, ConfigError), // loader name, config file, config error
    LoaderFailedError(String, String), // loader name, error description
    UnknownConfigDirectory,
}

pub type LoaderResult<T> = Result<Arc<T>,LoaderError>;

#[async_trait]
pub trait ModuleLoader : Sync + Send {
    fn name(&self) -> &str;
    async fn load(&self, config_path : &std::path::Path, incoming_message_transmitter : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module>;
}

#[async_trait]
pub trait Loader : Sync + Send {
    fn module_loader_by_name(&self, connector_name : &str) -> Option<&dyn ModuleLoader>;

    async fn load_module_from_file(&self, module_loader_name : &str, config_path : &std::path::Path, tx : Arc<Mutex<mpsc::Sender<Message>>>) -> LoaderResult<dyn Module> {
        match self.module_loader_by_name(module_loader_name) {
            Some(loader) => loader.load(config_path, tx).await,
            None => Err(LoaderError::LoaderDoesNotExist(module_loader_name.to_owned()))
        }
    }
    fn module_loader_name_list(&self) -> Vec<&str>;
}

pub struct DefaultLoader {
    module_loaders : Vec<Box<dyn ModuleLoader>>,
}

impl DefaultLoader{
    pub fn new(module_loaders : Vec<Box<dyn ModuleLoader>>) -> Self{
        DefaultLoader{module_loaders}
    }
}

impl Loader for DefaultLoader {
    fn module_loader_by_name(&self, module_name : &str) -> Option<&dyn ModuleLoader>{
        let loaders : &Vec<Box<dyn ModuleLoader>> = &self.module_loaders;
        loaders.iter()
               .filter(|l| l.name()==module_name)
               .map(|l| l.as_ref())
               .next()
    }
    fn module_loader_name_list(&self) -> Vec<&str> {
        self.module_loaders.iter()
                                     .map(|p| p.name())
                                     .collect()
    }
}

pub struct MetaLoader {
    loaders : Vec<Box<dyn Loader>>
}

impl MetaLoader {
    pub fn new(loaders : Vec<Box<dyn Loader>>) -> Self {
        MetaLoader {
            loaders : loaders
        }
    }
}

impl Loader for MetaLoader {
    fn module_loader_by_name(&self, module_name : &str) -> Option<&dyn ModuleLoader> {
        let loaders : Vec<&dyn ModuleLoader> = self.loaders
                                                      .iter()
                                                      .map(|l| l.module_loader_by_name(module_name))
                                                      .filter(|l| l.is_some())
                                                      .map(|l| l.unwrap())
                                                      .collect();
        match loaders.iter().next() {
            None => None,
            Some(i) => Some(*i)
        }
    }
    fn module_loader_name_list(&self) -> Vec<&str> {
        self.loaders.iter()
                    .flat_map(|l| l.module_loader_name_list())
                    .collect()
    }
}
