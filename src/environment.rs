use directories_next::ProjectDirs;
use std::sync::Arc;
use crate::Module;
use crate::attribute::AttributeValue;

pub struct Environment {
    profile_name : String,
    base_config_path : Option<std::path::PathBuf>,
    pub modules : Vec<Arc<dyn Module>>
}

impl Environment {
    pub fn default_base_config_path() -> Option<std::path::PathBuf> {
        match ProjectDirs::from("de", "n8zone", "Dashbot") {
            Some(project_dirs) => Some(project_dirs.config_dir().to_owned()),
            None => None
        }
    }
    pub fn base_config_path(&self) -> Option<std::path::PathBuf> {
        match &self.base_config_path {
            None => Self::default_base_config_path(),
            Some(path) => Some(path.clone())
        }
    }
    pub fn new(profile_name : &str, modules : Vec<Arc<dyn Module>>) -> Self {
        Environment {
            profile_name : profile_name.to_owned(),
            base_config_path : None,
            modules : modules
        }
    }
    pub fn new_overwrite_config_path(profile_name : &str,
                                     modules : Vec<Arc<dyn Module>>,
                                     base_config_path : &std::path::Path) -> Self {
        Environment {
            profile_name : profile_name.to_owned(),
            base_config_path : Some(base_config_path.to_owned()),
            modules : modules
        }
    }
    pub fn profile_name(&self) -> &str {
        &self.profile_name
    }
    pub async fn attribute(&self, attribute_name : &str) -> Option<AttributeValue> {
        for provider in self.modules.iter() {
            match provider.get_attribute_value(attribute_name).await {
                Some(attribute) => return Some(attribute),
                _ => {}
            }
        }
        None
    }
    pub fn config_path(&self) -> Option<std::path::PathBuf> {
        match self.base_config_path() {
            Some(base_config_path) => {
                let mut base_config_path = base_config_path.clone();
                base_config_path.push(&self.profile_name);
                Some(base_config_path)
            }
            None => None
        }
    }
}

