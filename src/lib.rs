pub mod implementations;
pub mod config;
pub mod environment;
pub mod loader;
pub mod profile;
pub mod attribute;
pub mod communication;

pub mod processing {
    #[derive(Debug)]
    pub enum ConnectorError {
        NotConnected,
        Rejected,
        NotImplemented,
        Other(String),
        UnknownError
    }
}

use  async_trait::*;
use crate::communication::*;
use crate::attribute::*;
use crate::environment::Environment;
use crate::processing::ConnectorError;

#[async_trait]
pub trait Module : Send + Sync {
    async fn process_command(&self, _command : &CommandMessage, _author : &User, _environment : &Environment) -> Option<Response> {
        None
    }
    async fn process_chat(&self, _message : &dyn ChatMessage, _author : &User, _environment : &Environment) -> Option<Response> {
        None
    }
    async fn send(&self, _response: &Response) -> Result<(),ConnectorError> {
        Err(ConnectorError::NotImplemented)
    }
    async fn run(&self) {
    }
    async fn attributes(&self) -> Vec<Attribute> {
        vec![]
    }
    async fn has_attribute(&self, attribute_name : &str) -> bool {
        let attributes = self.attributes().await;
        let attributes_filtered : Option<&Attribute> = attributes.iter()
                                                                 .filter(|a| a.name()==attribute_name)
                                                                 .next();
        attributes_filtered.is_some()
    }
    async fn get_attribute_value(&self, attribute_name : &str) -> Option<AttributeValue> {
        let attributes = self.attributes().await;
        let mut attributes_filtered = attributes.iter()
                                            .filter(|a| a.name()==attribute_name)
                                            .map(|a| a.value());
        match attributes_filtered.next() {
            Some(Some(attribute)) => Some(attribute.clone()),
            _ => None
        }
    }
    async fn update(&self) {
    }
}

