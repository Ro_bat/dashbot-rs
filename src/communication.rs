use serde::{Serialize, Deserialize};

#[derive(Eq, Ord, PartialEq, PartialOrd, Clone, Copy, Serialize, Deserialize, Debug)]
pub struct PowerLevel (u8);

impl PowerLevel {
    pub fn default() -> PowerLevel {
        PowerLevel(0)
    }
    pub fn moderator() -> PowerLevel {
        PowerLevel(50)
    }
    pub fn admin() -> PowerLevel {
        PowerLevel(100)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct User {
    pub name : String,
    pub platform : String,
    pub qualifier : String,
    pub power_level : PowerLevel
}

impl User {
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn platform(&self) -> &str {
        &self.platform
    }
    pub fn qualifier(&self) -> &str {
        &self.qualifier
    }
    pub fn power_level(&self) -> PowerLevel {
        self.power_level
    }
}

pub enum MessageType {
    ChatMessage(Box<dyn ChatMessage>),
    CommandMessage(CommandMessage)
}

pub struct Message {
    pub message : MessageType,
    pub author : User
}

pub trait ChatMessage : Send + Sync {
    fn text(&self) -> &str;
}

pub struct DefaultChatMessage {
    pub text : String
}

impl ChatMessage for DefaultChatMessage {
    fn text(&self) -> &str {
        &self.text
    }
}

pub struct CommandMessage {
    pub name : String,
    pub args : Vec<String>,
}

impl Message {
    pub fn from(text : &str, author : User, command_prefix : &str) -> Message {
        if command_prefix.len() > text.len() {
            Message {
                message : MessageType::ChatMessage(Box::new(DefaultChatMessage{
                    text: text.to_owned()
                })),
                author
            }
            
        } else if text.starts_with(command_prefix) {
            let mut text = text[command_prefix.len()..].split(" ");
            let command_name = match text.next() {
                Some(i) => i,
                None => ""
            };
            let args_string = text.collect::<Vec<&str>>().join(" ");
            let args = args_string.split("|").map(|a|a.trim().to_string()).collect();
            Message {
                message: MessageType::CommandMessage(CommandMessage {
                    name : command_name.to_owned(),
                    args : args,
                }),
                author
            }
        } else {
            Message {
                message: MessageType::ChatMessage(Box::new(DefaultChatMessage{
                    text: text.to_owned(),
                })),
                author
            }
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum Response {
    Text(String),
    NotAllowed(User),
    Ban(User, String) // user, reason
}

impl From<&str> for Response {
    fn from(text: &str) -> Self {
        Response::Text(text.to_owned())
    }
}

impl From<String> for Response {
    fn from(text: String) -> Self {
        Response::Text(text)
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Command {
    pub command : String,
    pub description : String,
    pub required_power_level : PowerLevel
}

impl Command {
    pub fn available_to(&self, user : &User) -> bool {
        user.power_level() >= self.required_power_level
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    pub fn test_user() -> User {
        User {
            name : "Test_User".to_string(),
            platform : "Testing".to_string(),
            qualifier : "test".to_string(),
            power_level : PowerLevel::moderator()
        }
    }
    pub fn test_user_power_level(power_level : PowerLevel) -> User {
        User {
            name : "Test_User".to_string(),
            platform : "Testing".to_string(),
            qualifier : "test".to_string(),
            power_level
        }
    }
    #[test]
    fn default_user_user_impl_test() {
        let user = test_user();
        assert_eq!(user.name(), "Test_User");
        assert_eq!(user.platform(), "Testing");
        assert_eq!(user.qualifier(), "test");
        assert_eq!(user.power_level(), PowerLevel::moderator());
    }
    #[test]
    fn command_message_parsing_is_command() {
        let user = test_user();
        let parsed_message = Message::from("!testcommand This is arg1 | This is arg2",
                                           user,
                                           "!");
        assert!(match parsed_message.message {
            MessageType::CommandMessage(_) => true,
            _ => false
        });
    }
    #[test]
    fn command_message_parsing_user_check() {
        let user = test_user();
        let parsed_message = Message::from("!testcommand This is arg1 | This is arg2",
                                           user,
                                           "!");
        let user = test_user();
        assert!(parsed_message.author == user);
    }
    #[test]
    fn command_message_parsing_command_name_check() {
        let user = test_user();
        let parsed_message = Message::from("!testcommand This is arg1 | This is arg2",
                                           user,
                                           "!");
        assert!(match parsed_message.message {
            MessageType::CommandMessage(command_message) => {
                command_message.name == "testcommand"
            },
            _ => false
        });
    }
    #[test]
    fn command_message_parsing_args_check() {
        let user = test_user();
        let parsed_message = Message::from("!testcommand This is arg1 | This is arg2",
                                           user,
                                           "!");
        assert!(match parsed_message.message {
            MessageType::CommandMessage(command_message) => {
                println!("{}",command_message.name);
                command_message.args == vec!["This is arg1","This is arg2"]
            },
            _ => false
        });
    }
    #[test]
    fn chat_message_parsing_is_chat() {
        let user = test_user();
        let parsed_message = Message::from("This is a regular chat message.",
                                           user,
                                           "!");
        assert!(match parsed_message.message {
            MessageType::ChatMessage(_) => true,
            _ => false
        });
    }
    #[test]
    fn chat_message_parsing_content_check() {
        let user = test_user();
        let parsed_message = Message::from("This is a regular chat message.",
                                           user,
                                           "!");
        assert!(match parsed_message.message {
            MessageType::ChatMessage(chat_message) => chat_message.text() == "This is a regular chat message.",
            _ => false
        });
    }
    #[test]
    fn command_privilege_check_default() {
        let default_command = Command {
            command: "test".to_owned(),
            description: "".to_owned(),
            required_power_level: PowerLevel::default()
        };
        let default = test_user_power_level(PowerLevel::default());
        let moderator = test_user_power_level(PowerLevel::moderator());
        let admin = test_user_power_level(PowerLevel::admin());
        assert!(default_command.available_to(&default));
        assert!(default_command.available_to(&moderator));
        assert!(default_command.available_to(&admin));
    }
    #[test]
    fn command_privilege_check_moderator() {
        let moderator_command = Command {
            command: "test".to_owned(),
            description: "".to_owned(),
            required_power_level: PowerLevel::moderator()
        };
        let default = test_user_power_level(PowerLevel::default());
        let moderator = test_user_power_level(PowerLevel::moderator());
        let admin = test_user_power_level(PowerLevel::admin());
        assert!(!moderator_command.available_to(&default));
        assert!(moderator_command.available_to(&moderator));
        assert!(moderator_command.available_to(&admin));
    }
    #[test]
    fn command_privilege_check_admin() {
        let admin_command = Command {
            command: "test".to_owned(),
            description: "".to_owned(),
            required_power_level: PowerLevel::admin()
        };
        let default = test_user_power_level(PowerLevel::default());
        let moderator = test_user_power_level(PowerLevel::moderator());
        let admin = test_user_power_level(PowerLevel::admin());
        assert!(!admin_command.available_to(&default));
        assert!(!admin_command.available_to(&moderator));
        assert!(admin_command.available_to(&admin));
    }
}
