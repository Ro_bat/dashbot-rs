use dashbot::implementations::*;
use dashbot::loader::*;
use dashbot::profile::ProfileDataCollection;
use dashbot::environment::Environment;
use dashbot::config::Configurable;
use tokio;
use tokio::sync::Mutex;
use std::sync::Arc;

#[tokio::main(worker_threads = 32)]
async fn main() -> () {
    let loader = Arc::new(Mutex::new(module_loader())) as Arc<Mutex<Box<dyn Loader>>>;
    println!("modules: {:?}", loader.lock().await.module_loader_name_list());
    let mut profile_config_file = Environment::default_base_config_path().expect("Could not determine base config path.");
    profile_config_file.push("profiles.yaml");
    let profile_collection = ProfileDataCollection::load_from_file(&profile_config_file).expect(&format!("An error occured trying to load '{}'.", profile_config_file.to_str().unwrap()));
    profile_collection.run(loader).await;
}
