use directories_next::ProjectDirs;
use sanitize_filename;
use serde::{Serialize};
use serde::de::DeserializeOwned;
use std::fs::{self};
use std::io::{BufReader};
use serde_yaml;

#[derive(Debug)]
pub enum ConfigError {
    IoError(std::io::Error),
    SerdeError(serde_yaml::Error),
    ConfigurationDirNotFound
}

impl From<std::io::Error> for ConfigError {
    fn from(error: std::io::Error) -> Self {
        ConfigError::IoError(error)
    }
}

impl From<serde_yaml::Error> for ConfigError {
    fn from(error: serde_yaml::Error) -> Self {
        ConfigError::SerdeError(error)
    }
}

fn base_config_directory() -> Option<std::path::PathBuf> {
    if let Some(project_dirs) = ProjectDirs::from("de", "n8zone",  "Dashbot") {
        let path = project_dirs.config_dir().clone();
        let path_owned = path.to_owned();
        Some(path_owned)
    } else {
        None
    }
}

fn profile_config_directory(profile : &str) -> Option<std::path::PathBuf> {
    match base_config_directory() {
        None => None,
        Some(path) => {
            let mut path = path.clone();
            path.push(profile);
            Some(path)
        }
    }
}

pub fn config_file(profile : &str, file_name : &str) -> Option<std::path::PathBuf> {
    match profile_config_directory(profile) {
        None => None,
        Some(path) => {
            let mut path = path.clone();
            path.push(file_name);
            path.set_extension("yaml");
            Some(path)
        }
    }
}

pub trait Configurable : Serialize + DeserializeOwned + Default + Sized {
    fn config_file_name() -> String {
        String::from(std::any::type_name::<Self>())
    }

    fn sanitize_options() -> &'static sanitize_filename::Options<'static> {
        static OPTIONS : sanitize_filename::Options = sanitize_filename::Options {
            truncate: true,
            windows: true,
            replacement: "_"
        };
        &OPTIONS
    }

    fn load_from_file(file_path : &std::path::Path) -> Result<Self, ConfigError> {
        if file_path.exists() {
            let file = fs::File::open(file_path)?;
            let buf_reader = BufReader::new(file);
            match serde_yaml::from_reader(buf_reader) {
                Ok(instance) => Ok(instance),
                Err(_err) => Err(ConfigError::SerdeError(_err))
            }
        } else {
            let default = Self::default();
            let data = serde_yaml::to_string(&default);
            match data {
                Ok(data) => {
                    match file_path.parent() {
                        Some(path) => {fs::create_dir_all(path)?;}
                        _ => ()
                    }
                    fs::write(file_path,data)?;
                    Ok(default)
                }
                Err(err) => Err(ConfigError::SerdeError(err))
            }
        }
    }

    fn load_from_config(profile : &str) -> Result<Self, ConfigError> {
        let file_name = Self::config_file_name();
        let sanitized_filename = sanitize_filename::sanitize_with_options(file_name, Self::sanitize_options().clone());
        let file_path = config_file(profile, &sanitized_filename);
        match file_path {
            Some(path) => Self::load_from_file(&path),
            None => Err(ConfigError::ConfigurationDirNotFound)
        }
    }

    fn save_to_config(&self, profile : &str) -> Result<(), ConfigError> {
        let file_name = Self::config_file_name();
        let sanitized_filename = sanitize_filename::sanitize_with_options(file_name, Self::sanitize_options().clone());
        let file_path = config_file(profile, &sanitized_filename);
        match file_path {
            Some(path) => self.save_to_file(&path),
            None => Err(ConfigError::ConfigurationDirNotFound)
        }
    }

    fn save_to_file(&self, file_path : &std::path::Path) -> Result<(), ConfigError> {
        let data = serde_yaml::to_string(self);
        match data {
            Ok(data) => {
                fs::write(file_path,data)?;
                Ok(())
            }
            Err(err) =>  Err(ConfigError::SerdeError(err))
        }
    }
}
